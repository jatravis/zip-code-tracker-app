# Zip Code Tracker

Simple REST API for keeping a list of zip codes

## Setup

In the project directory, run `yarn install` or `npm install` to install the project dependencies.

Then run: `yarn start` or `npm run start` to run the application.

If you'd like to run the application in development mode, you can use `yarn dev` or `npm run dev` instead.

All of the endpoints can now be reached at [http://localhost:3000](http://localhost:3000)

## Endpoints

- Display (GET)
    - Displays a list of all zip codes currently in the system
    - Example Request: `http://localhost:3000/display`
    - Example Response: `{ "value": "10000, 10002, 20245-20247, 45021 }`
- Has (GET)
    - Returns true if the zip code exists in the system, false if 
    - Example Request: `http://localhost:3000/has/12345`
    - Example Response: `{  "value: ": true }`
- Insert (POST)
    - Displays a list of all zip codes currently in the system
    - Example Request: `http://localhost:3000/insert/12345`
    - Example Response: `{ "value": "12345 was inserted" }`
- Delete (DELETE)
    - Displays a list of all zip codes currently in the system
    - Example Request: `http://localhost:3000/delete/12345`
    - Example Response: `{ "value": "12345 was deleted" }`

## Tip

If you delete or mess up the `zip_codes.json` you can generate a new file by running `yarn generate` or `npm run generate`
