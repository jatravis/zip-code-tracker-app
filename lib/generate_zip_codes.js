const fs = require('fs');
const zipCodesObject = {
    "0": ["00000"],
    "1": ["10000"],
    "2": ["20000"],
    "3": ["30000"],
    "4": ["40000"],
    "5": ["50000"],
    "6": ["60000"],
    "7": ["70000"],
    "8": ["80000"],
    "9": ["90000"]
}

fs.writeFile('./zip_codes.json', JSON.stringify(zipCodesObject), () => {
    return process.exit(0);
});
