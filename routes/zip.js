const express = require('express');
const router = express.Router();

//Validation Middleware
const validateZipCode = require('../middleware/validate_zip_code');

//Lib Imports
const hasZipCode = require('../lib/has_zip_code');
const displayZipCodes = require('../lib/display_zip_codes');
const insertZipCode = require('../lib/insert_zip_code');
const deleteZipCode = require('../lib/delete_zip_code');

// Insert - Add a new zip code to the list.
// Delete - Remove a zip code from the list.
// Has - Determines if a zip code exists in the list. This operation must have an average case runtime complexity of O(1).
// Display - Shows the full list of zip codes with contiguous ranges grouped in a shortened form (see example below).

router.get('/display', validateZipCode, async (req, res) => {
    try{
        const returnString = await displayZipCodes();
        res.status(200).json({value: returnString});
    } catch(err){
        res.status(500).json({error: err.message});
    }
});

router.get('/has/:zip_code',validateZipCode, async (req, res) => {
    try{
        const returnString = await hasZipCode(req.params.zip_code);
        res.status(200).json({value: returnString});
    } catch(err){
        res.status(500).json({error: err.message});
    }
});

router.post('/insert/:zip_code', validateZipCode, async (req, res) => {
    try{
        const returnString = await insertZipCode(req.params.zip_code);
        res.status(201).json({value: returnString});
    } catch(err){
        res.status(500).json({error: err.message});
    }
});

router.delete('/delete/:zip_code', validateZipCode, async (req, res) => {
    try{
        const returnString = await deleteZipCode(req.params.zip_code);
        res.status(200).json({value: returnString})
    } catch(err){
        res.status(500).json({error: err.message});
    }
});

router.all('*', (req, res) => {
    res.status(404).json({value: "Route does not exist."});
  });

module.exports = router;