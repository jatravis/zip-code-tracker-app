const fsPromises = require('fs/promises');
const getAllZipCodes = require('./get_all_zip_codes');
const insertionSort = require('./insertion_sort')

/**
 * Inserts a new zip code into the system
 * @param {string} zipCode The new zip code to be added
 * @returns {string} Confirmation string
 */
const insertZipCode = async (zipCode) => {
    let zipCodesObject;
    
    try{
        zipCodesObject = await getAllZipCodes();
    } catch(err){
        throw err;
    }

    const firstChar = zipCode.charAt(0);
    const correlatingArray = zipCodesObject[firstChar];
    
    if(correlatingArray.includes(zipCode)){
        throw new Error(`${zipCode} already exists in system. Could not be inserted`);
    }
    
    correlatingArray.push(zipCode);
    insertionSort(correlatingArray);
    
    zipCodesObject[firstChar] = correlatingArray;

    try{
        await fsPromises.writeFile('./zip_codes.json', JSON.stringify(zipCodesObject));
    } catch(err){
        throw new Error(`${zipCode} could not be inserted.`)
    }
    
    return `${zipCode} was inserted`;
};

module.exports = insertZipCode;