const insertionSort = (zipCodeArray) => {
    for(let i=0; i<zipCodeArray.length; i++){
        let pivot = zipCodeArray[i];
        let j = i-1;

        while(j >= 0 && zipCodeArray[j] > pivot){
            zipCodeArray[j+1] = zipCodeArray[j];
            j = j-1;
        }

        zipCodeArray[j+1] = pivot;
    }
}

module.exports = insertionSort;