const fsPromises = require('fs/promises');
/**
 *  Returns all zip codes within the system as either an array or the original object
 * @param {boolean} asArray Boolean to switch return type
 * @returns {(Object.<string,string>|Array)} Object or Array of zip codes
 */
const getAllZipCodes = async (asArray = false) => {
    let zipCodes;
    try{
        zipCodes = await fsPromises.readFile('./zip_codes.json', 'utf-8');
    } catch(err){
        throw new Error("Please don't touch the zip_codes.json, it's fragile.")
    }

    return JSON.parse(zipCodes);
}

module.exports = getAllZipCodes;