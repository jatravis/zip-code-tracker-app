const fsPromises = require('fs/promises');
const getAllZipCodes = require('./get_all_zip_codes');
/**
 * Deletes a zip code from the system
 * @param {string} zipCode The zip code to be deleted
 * @returns {string} Confirmation string
 */
const deleteZipCode = async (zipCode) => {
    let zipCodesObject;

    try {
        zipCodesObject = await getAllZipCodes();
    } catch (err) {
        throw err;
    }
    const firstChar = zipCode.charAt(0);
    const correlatingArray = zipCodesObject[firstChar];

    if (!correlatingArray.includes(zipCode)) {
        throw new Error (`${zipCode} does not exist in the system. Could not be deleted.`);
    }

    const zipIndex = correlatingArray.indexOf(zipCode);
    correlatingArray.splice(zipIndex, 1);

    zipCodesObject[firstChar] = correlatingArray;
    try {
        await fsPromises.writeFile('./zip_codes.json', JSON.stringify(zipCodesObject));
    } catch (err) {
        throw new Error(`${zipCode} could not be deleted.`);
    }

    return `${zipCode} was deleted.`;
};

module.exports = deleteZipCode;