const validateZipCode = (req, res, next) => {
    const regEx = /^([0-9]{5})$/;

    if(req.params.zip_code){
        if(regEx.exec(req.params.zip_code)){
            next();
        } else {
            res.status(400).json({value: "Invalid Zip Code"});
        }
    } else{
        next();
    }
    
};

module.exports = validateZipCode;