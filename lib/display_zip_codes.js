const getAllZipCodes = require('./get_all_zip_codes');

/**
 * Returns all of the zip codes in the system
 * @returns {string} Formatted zip codes
 */
const displayZipCodes = async () => {
    let zipCodesObject;
    
    try{
        zipCodesObject = await getAllZipCodes();
    } catch(err){
        throw err;
    }

    let zipCodesArray = []

    Object.values(zipCodesObject).forEach(arr => {
        zipCodesArray = zipCodesArray.concat(arr);
    })
    
    if(zipCodesArray.length === 0){
        return "There are currently no zip codes in the system";
    }

    let returnString = `${zipCodesArray[0]}`;

    //Temp variables needed for zip concat
    let tempFlag = false;
    let currentNum = parseInt(zipCodesArray[0]);
    
    for(let i = 1; i<zipCodesArray.length; i++){
        if(++currentNum === (parseInt(zipCodesArray[i]))){
            if(!tempFlag){
                returnString += '-'
                tempFlag = true;
            } 
        } else {
            if(tempFlag){
                returnString += `${zipCodesArray[i-1]}, ${zipCodesArray[i]}`
            } else {
                returnString += `, ${zipCodesArray[i]}`
            }

            tempFlag = false;
            currentNum = parseInt(zipCodesArray[i]);
        }
    }
    
    return returnString;
}

module.exports = displayZipCodes;