const getAllZipCodes = require('./get_all_zip_codes');

/**
 * Searches the system for a single zip code
 * @param {string} zipCode The zip code to be found
 * @returns {boolean} Returns true if the zip code is found
 */
const hasZipCode = async (zipCode) => {
    let zipCodesSet;
    
    try{
        let zipCodesArray = [];
        
        Object.values(await getAllZipCodes()).forEach(arr => {
            zipCodesArray = zipCodesArray.concat(arr);
        })
        zipCodesSet = new Set(zipCodesArray);
    } catch(err){
        throw err;
    }

    //This probably doesn't count at O(1) after the conversion to the Set....
    return zipCodesSet.has(zipCode);
}

module.exports = hasZipCode;