const express = require('express');
const app = express();

//Route Imports
const zipRouter = require('./routes/zip');

app.use('/', zipRouter);

const port = 3000;
app.listen(port);